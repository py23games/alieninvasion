import pygame.font
from pygame.sprite import Group
from ship import Ship

class Scoreboard:

    def __init__(self, ai_game):
        self.ai_game = ai_game
        self.screen = ai_game.screen
        self.screen_rect = self.screen.get_rect()
        self.settings = ai_game.settings
        self.stats = ai_game.stats

        # Font configuration
        self.text_color = (30, 30, 30)
        self.font = pygame.font.SysFont(None, 46)

        '''Prepare image with starting score'''
        self.prepare_score()
        self.prepare_high_score()
        self.prepare_level()
        self.prepare_ships()

    def prepare_score(self):
        '''Transform score to image'''
        rounded_score = round(self.stats.score, -1)
        score_to_str = "{:,}".format(rounded_score)
        self.score_image = self.font.render(score_to_str, True, self.text_color, self.settings.bg_colour)

        '''View score on top-right corner'''
        self.score_rect = self.score_image.get_rect()
        self.score_rect.right = self.screen_rect.right - 23
        self.score_rect.top = 23

    def prepare_high_score(self):
        '''Transform high_score to image'''
        rounded_high_score = round(self.stats.high_score, -1)
        high_score_to_str = "{:,}".format(rounded_high_score)
        self.high_score_image = self.font.render(high_score_to_str, True, self.text_color, self.settings.bg_colour)

        '''View high_score on top-right corner'''
        self.high_score_rect = self.high_score_image.get_rect()
        self.high_score_rect.left = self.screen_rect.centerx
        self.high_score_rect.top = self.score_rect.top

    def prepare_level(self):
        '''Transform level to image'''
        level_to_str = str(self.stats.level)
        self.level_image = self.font.render(level_to_str, True, self.text_color, self.settings.bg_colour)

        '''Place level under the score'''
        self.level_rect = self.level_image.get_rect()
        self.level_rect.right = self.score_rect.right
        self.level_rect.top = self.score_rect.bottom + 10

    def prepare_ships(self):
        '''Show how many ships you have'''
        self.ships = Group()
        for ship_number in range(self.stats.ships_left):
            ship = Ship(self.ai_game)
            ship.rect.x = 10 + ship_number * ship.rect.width
            ship.rect.y = 10
            self.ships.add(ship)

    def show_score(self):
        '''Draw score on screen'''
        self.screen.blit(self.score_image, self.score_rect)
        self.screen.blit(self.high_score_image, self.high_score_rect)
        self.screen.blit(self.level_image, self.level_rect)
        self.ships.draw(self.screen)

    def check_high_score(self):
        if self.stats.score > self.stats.high_score:
            self.stats.high_score = self.stats.score
            self.prepare_high_score()
