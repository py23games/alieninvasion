class Settings:
    '''Class for saving game settings'''

    def __init__(self):
        '''Initialize settings'''
        self.screen_width = 1200
        self.screen_height = 800
        self.bg_colour = (25, 25, 112)
        self.ship_limit = 3

        '''Bullets settings'''
        self.bullet_width = 3
        self.bullet_height = 15
        self.bullet_colour = (255, 0, 0)
        self.bullets_allowed = 3

        '''Alien settings'''
        self.fleet_drop_speed = 10

        self.speed_up_scale = 1.1
        self.score_scale = 1.23
        self.initialize_dynamic_settings()

    def initialize_dynamic_settings(self):
        self.alien_speed = 0.5
        self.ship_speed = 0.5
        self.bullet_speed = 1.5

        # fleet direction 1 => right \ -1 => left
        self.fleet_direction = 1

        '''Score'''
        self.alien_points = 23
        
    def increase_speed(self):
        self.alien_speed *= self.speed_up_scale
        self.bullet_speed *= self.speed_up_scale
        self.ship_speed *= self.speed_up_scale

        self.alien_points = int(self.alien_points * self.score_scale)
