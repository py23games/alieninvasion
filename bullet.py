import pygame
from pygame.sprite import Sprite


class Bullet(Sprite):
    '''Class for manipulating bullets'''

    def __init__(self, ai_game):
        '''Creating Bullet object in current ship position'''
        super().__init__()
        self.screen = ai_game.screen
        self.settings = ai_game.settings
        self.colour = self.settings.bullet_colour

        '''Create Bullet rect in (0,0) and set correct position'''
        self.rect = pygame.Rect(
            0, 0, self.settings.bullet_width, self.settings.bullet_height)
        self.rect.midtop = ai_game.ship.rect.midtop

        '''Save position'''
        self.y = float(self.rect.y)

    def update(self):
        '''Move bullet to top'''
        self.y -= self.settings.bullet_speed
        # and save new
        self.rect.y = self.y  # type: ignore

    def draw_bullet(self):
        '''Draw bullets on screen'''
        pygame.draw.rect(self.screen, self.colour, self.rect)  # type: ignore
