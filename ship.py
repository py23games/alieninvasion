import pygame
from pygame.sprite import Sprite

class Ship(Sprite):
    def __init__(self, ai_game):
        super().__init__()
        '''Initialize ship at start position'''
        self.screen = ai_game.screen
        self.settings = ai_game.settings
        self.screen_rect = ai_game.screen.get_rect()

        '''Ship image and rect'''
        image = pygame.image.load('./images/ship.bmp')
        self.image = pygame.transform.scale(image, (32, 32))
        self.rect = self.image.get_rect()

        '''Create each new ship at middle-bottom on screen'''
        self.rect.midbottom = self.screen_rect.midbottom

        '''Save float position'''
        self.x = float(self.rect.x)

        '''Moves'''
        self.moving_right = False
        self.moving_left = False

    def update(self):
        '''Change image size'''
        '''Update ship position'''
        if self.moving_right and self.rect.right < self.screen_rect.right:
            self.x += self.settings.ship_speed
        elif self.moving_left and self.rect.left > 0:
            self.x -= self.settings.ship_speed

        self.rect.x = int(self.x)

    def blitme(self):
        '''Draw ship at current position'''
        self.screen.blit(self.image, self.rect)

    def center_ship(self):
        '''Centering ship on screen'''
        self.rect.midbottom = self.screen_rect.midbottom
        self.x = float(self.rect.x)
