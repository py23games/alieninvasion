import pygame.font

class Button:

    def __init__(self, ai_game, msg):
        '''Button attributes Initialization'''
        self.screen = ai_game.screen
        self.screen_rect = self.screen.get_rect()

        # Button params
        self.width, self.height = 200, 50
        self.button_color = (0, 255, 0)
        self.text_color = (255,255,255)
        self.font = pygame.font.SysFont(None, 46)

        # Create button rect and center it
        self.rect = pygame.Rect(0, 0, self.width, self.height)
        self.rect.center = self.screen_rect.center

        # Show message once
        self._prep_msg(msg)

    def _prep_msg(self, msg):
        '''Transform text to image, and put it to the center'''
        self.msg_image = self.font.render(msg, True, self.text_color, self.button_color)
        self.msg_image_rect = self.msg_image.get_rect()
        self.msg_image_rect.center = self.rect.center

    def draw_button(self):
        '''Draw empty button, and then message'''
        self.screen.fill(self.button_color, self.rect)
        self.screen.blit(self.msg_image, self.msg_image_rect)
